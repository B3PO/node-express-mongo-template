// GET ALL REQUEST -- Works
function getAll() {
  axios({
    method: 'get',
    url: 'http://localhost:8080/api/demos/'
  })
  .then(res => showOutput(res))
  .catch(err => console.log(err));
}

// GET SINGLE REQUEST -- Works
function getById() {
  let input = document.getElementById('valId').value;
  if (input === null || input === ''){
    alert('Please enter valid id.');
  }
  else {
    axios({
      method: 'get',
      url: `http://localhost:8080/api/demos/${input}`
    })
    .then(res => showOutput(res))
    .catch(err => console.log(err));  
  }
  
}

// DELETE REQUEST -- Works
function deleteAll() {
  axios
    .delete(`http://localhost:8080/api/demos/`)
    .then(res => showOutput(res))
    .catch(err => console.log(err));
}

// DELETE REQUEST -- Works
function deleteById() {
  let input = document.getElementById('valId').value;
  if (input === null || input === ''){
    alert('Please enter valid id.');
  }
  else {
    axios
      .delete(`http://localhost:8080/api/demos/${input}`)
      .then(res => showOutput(res))
      .catch(err => console.log(err));
  }
}


// POST REQUEST
function post() {
  //console.log('Post Request');
  let title = document.getElementById('newTitle').value;
  let description = document.getElementById('newDesc').value;
  let published = document.getElementById('newPub').value;
  axios
    .post('http://localhost:8080/api/demos/', {
      title: title,
      description: description,
      published: published
    })
    .then(res => showOutput(res))
    .catch(err => console.log(err));

}

// PUT/PATCH REQUEST
function updateById() {
  let input = document.getElementById('valId').value;
  if (input === null || input === ''){
    alert('Please enter valid id.');
  }
  else {
    let title = document.getElementById('upTitle').value;
    let description = document.getElementById('upDesc').value;
    let published = document.getElementById('upPub').value;
    axios
      .put(`http://localhost:8080/api/demos/${input}`, {
        title: title,
        description: description,
        published: published,
      })
      .then(res => showOutput(res))
      .catch(err => console.log(err));
  }
}



// SIMULTANEOUS DATA
function getData() {
  console.log('Simultaneous Request');
}

// CUSTOM HEADERS
function customHeaders() {
  console.log('Custom Headers');
}

// TRANSFORMING REQUESTS & RESPONSES
function transformResponse() {
  console.log('Transform Response');
}

// ERROR HANDLING
function errorHandling() {
  console.log('Error Handling');
}

// CANCEL TOKEN
function cancelToken() {
  console.log('Cancel Token');
}

// INTERCEPTING REQUESTS & RESPONSES
function interceptRequestResponse() {
  console.log('Interceptin Requests & Responses');
}

// AXIOS INSTANCES
function instances() {
  console.log('Axios Instances');
}

// Show output in browser
function showOutput(res) {
  document.getElementById('res').innerHTML = `
  <div class="card card-body mb-4">
    <h5>Status: ${res.status}</h5>
  </div>

  <div class="card mt-3">
    <div class="card-header">
      Headers
    </div>
    <div class="card-body">
      <pre style="color:white">${JSON.stringify(res.headers, null, 2)}</pre>
    </div>
  </div>

  <div class="card mt-3">
    <div class="card-header">
      Data
    </div>
    <div class="card-body">
      <pre style="color:white">${JSON.stringify(res.data, null, 2)}</pre>
    </div>
  </div>

  <div class="card mt-3">
    <div class="card-header">
      Config
    </div>
    <div class="card-body">
      <pre style="color:white">${JSON.stringify(res.config, null, 2)}</pre>
    </div>
  </div>
`;
}

// Event listeners

document.getElementById('getAll').addEventListener('click', getAll);
document.getElementById('get').addEventListener('click', getById);
document.getElementById('post').addEventListener('click', post);
document.getElementById('update').addEventListener('click', updateById);
document.getElementById('delete').addEventListener('click', deleteById);
document.getElementById('deleteAll').addEventListener('click', deleteAll);


document.getElementById('sim').addEventListener('click', getData);
document.getElementById('headers').addEventListener('click', customHeaders);
document.getElementById('transform').addEventListener('click', transformResponse);
document.getElementById('error').addEventListener('click', errorHandling);
document.getElementById('cancel').addEventListener('click', cancelToken);

