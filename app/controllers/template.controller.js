const db = require("../models");
const Demo = db.demos;
// Create and Save a new demo
exports.create = (req, res) => {
// Validate request
  if (!req.body.title) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }
  // Create a demo
  const demo = new Demo({
    title: req.body.title,
    description: req.body.description,
    published: req.body.published ? req.body.published : false
  });
  // Save demo in the database
  demo
    .save(demo)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the demo."
      });
    });
};
// Retrieve all demos from the database.
exports.findAll = (req, res) => {
    const title = req.query.title;
    var condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};
    Demo.find(condition)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving demos."
        });
    });
};
// Find a single demo with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    Demo.findById(id)
        .then(data => {
        if (!data)
            res.status(404).send({ message: "Not found demo with id " + id });
        else res.send(data);
        })
        .catch(err => {
        res
            .status(500)
            .send({ message: "Error retrieving demo with id=" + id });
        });
};
// Update a demo by the id in the request
exports.update = (req, res) => {
        if (!req.body) {
          return res.status(400).send({
            message: "Data to update can not be empty!"
          });
        }
        const id = req.params.id;
        Demo.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
          .then(data => {
            if (!data) {
              res.status(404).send({
                message: `Cannot update demo with id=${id}. Maybe demo was not found!`
              });
            } else res.send({ message: "demo was updated successfully." });
          })
          .catch(err => {
            res.status(500).send({
              message: "Error updating demo with id=" + id
            });
          });
};
// Delete a demo with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    Demo.findByIdAndRemove(id)
        .then(data => {
        if (!data) {
            res.status(404).send({
            message: `Cannot delete demo with id=${id}. Maybe demo was not found!`
            });
        } else {
            res.send({
            message: "Demo was deleted successfully!"
            });
        }
        })
        .catch(err => {
        res.status(500).send({
            message: "Could not delete Demo with id=" + id
        });
    });
};
// Delete all demos from the database.
exports.deleteAll = (req, res) => {
    Demo.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Demos were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all demos."
      });
    });
};
// Find all published demos
exports.findAllPublished = (req, res) => {
    Demo.find({ published: true })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving demos."
      });
    });
};