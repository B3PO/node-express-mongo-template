module.exports = mongoose => {
    const Demo = mongoose.model(
      "demo",
      mongoose.Schema(
        {
          title: String,
          description: String,
          published: Boolean
        },
        { timestamps: true }
      )
    );
    return Demo;
  };