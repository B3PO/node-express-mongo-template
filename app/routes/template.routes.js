module.exports = app => {
    const demos = require("../controllers/template.controller.js");
    var router = require("express").Router();
    // Create a new demo
    router.post("/", demos.create);
    // Retrieve all demos
    router.get("/", demos.findAll);
    // Retrieve all published demos
    router.get("/published", demos.findAllPublished);
    // Retrieve a single demo with id
    router.get("/:id", demos.findOne);
    // Update a demo with id
    router.put("/:id", demos.update);
    // Delete a demo with id
    router.delete("/:id", demos.delete);
    // Create a new demo
    router.delete("/", demos.deleteAll);
    app.use('/api/demos', router);
};