# node-express-mongo-template

Template for rapid prototyping Node, Express, Mongoose and MongoDB. Includes JavaScript frontend for testing locally. 

## Getting started

Before running npm install && npm start, open db.config.js and replace the URL connection string with the connection string on your mongodb.

```
cd programming-folder/
git clone https://gitlab.com/c6298/node-express-mongo-template.git
cd node-express-mongo-template
npm install && npm start
```

Open frontend-testing folder and open index.html for local testing.
* CORS extension needed for local testing.





